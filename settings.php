<?php
//require_once('common/includes/class.http.php');
require_once('common/includes/class.httprequest.php');
require_once('common/admin/admin_menu.php');

$version = "2.8";

/*$html .= "
<script src='http://code.jquery.com/jquery.min.js' type='text/javascript'></script>
<script type='text/javascript'>
$(document).ready(function(){
	$.getJSON('http://www.elementstudio.co.uk/downloads/v.json', function(data) {

	alert('here');

	})
.success(function() { alert('second success'); })
.error(function() { alert('error'); })
.complete(function() { alert('complete'); });
});
</script>";*/

$page = new Page('Ship Display tool - Settings');

$html .= "<div class='block-header2'>Ship Display Tool Admin page.</div><br />Created by Spark's.<br />Maintained by Salvoxia.<br />Modded Version by Snitch Ashor.<br />Enjoy.<br />";

if ($_POST['submit']) {
  $tool_back = $_POST["sel_back"];
  $tool_pods = ($_POST["fit_pods"]) ? 1 : 0;
  $tool_webgl = ($_POST["webgl"]) ? 1 : 0;
  $tool_webgl_ol = ($_POST["webgl_ol"]) ? 1 : 0;


  config::set('ship_display_back', $tool_back);
  config::set('ship_display_pods', $tool_pods);
  config::set('ship_display_webgl', $tool_webgl);
  config::set('ship_display_webgl_ol', $tool_webgl_ol);
//  Header("Location: ?a=settings_ship_display_tool");
}

if ($_POST['updatedb']) {
  $sqlSource = file('mods/ship_display_tool/Sql/type2graphic.sql');
  $dbq = DBFactory::getDBQuery();
  foreach ($sqlSource as $line)
  {
    if (substr($line, 0, 2) == '--' || $line == '')
      continue;
    $templine .= $line;
    if (substr(trim($line), -1, 1) == ';')
    {
      $dbq->execute($templine);
      $templine = '';
    }
  }
  $html .= "Imported graphic ressource tables.<br /><br />";
}

if ($_POST['dropdb'])
{
        $dbq = DBFactory::getDBQuery();
        $drop_sql1 = "DROP TABLE IF EXISTS yamlGraphicIDs";
        $drop_sql2 = "DROP TABLE IF EXISTS yamlTypeIDs";
        $drop_sql3 = "DROP TABLE IF EXISTS type2graphic";
        $dbq->execute($drop_sql1);
        $dbq->execute($drop_sql2);
        $dbq->execute($drop_sql3);
	$html .= "Graphic ressource tables dropped.";
}


$backgroundimg = config::get('ship_display_back');
if($backgroundimg == "") {
	$backgroundimg = "#222222";
}
$html .= "<br /><div class='block-header2'>Settings</div>
<form name=options id=options method=post action=>
	<div style='float:left; width:100%;'>Select your mod background colour in hash, Example: #ffffff: <input type='text' name='sel_back' value='".$backgroundimg."' /></div>";
$html .= "<div style='float:left; width:100%;'>Fit Implants to Pods:<input type=checkbox name='fit_pods' id='fit_pods'";
if (config::get('ship_display_pods'))
{
    $html .= " checked=\"checked\"";
}
$html .= "      /><br /></div>";
$html .= "<div style='float:left; width:100%;'>Use WebGl ship Display:<input type=checkbox name='webgl' id='webgl'";
if (config::get('ship_display_webgl'))
{
    $html .= " checked=\"checked\"";
}
$html .= "      /><br /></div>";
$html .= "<div style='float:left; width:100%;'>Display WebGl Hardeners/Boosting:<input type=checkbox name='webgl_ol' id='webgl_ol'";
if (config::get('ship_display_webgl_ol'))
{
    $html .= " checked=\"checked\"";
}

$html .= "	/><br /><br /></div>
<div style='float:left; width:100%;'><input type=submit name=updatedb value=\"Install/Update DB\" /> Required for the WebGL Feature to work.</div>
<div style='float:left; width:100%;'><input type=submit name=dropdb value=\"Drop Tables again\" /> Remove the Tables required for the WebGL feature.</div>
<div style='float:left; width:100%;'><input type=submit name=submit value=\"Save\" /></div>
</form>
";




$html .= "<br /><br />Please find the general support thread <a href='http://evekb.org/forum/viewtopic.php?f=505&t=21873'>here</a>.<br />
Send Issues reagrding the Pod display or the WebGL feature to prometh on the EDk forum or Snitch Ashor ingame.
<br /><br />Thanks.";


$page->setContent($html);
$page->addContext($menubox->generate());
$page->generate();
